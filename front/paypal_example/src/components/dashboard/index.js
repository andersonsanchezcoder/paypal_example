import React, { Component } from 'react';
import './dashboard.scss';
import DashboardInfoFetcher from '../../services/dashboardInfoFetcher';


class Dashboard extends Component {

  constructor(){
    super();
    this.state = {
      balance: null
    };
  }

  componentDidMount() {
    DashboardInfoFetcher()
    .then(response =>{
      console.log(response);
      this.setState({ balance: response.data.balance })
    });
  }
  
  render() {
    return (
      <div className="main">
        <h1>Bienvenido al Dashboard</h1>
        <h3>Usted tiene un balance de:  { this.state.balance === null ?  'N/A'
                                          : this.state.balance }</h3>
      </div>
    );
  }
}

export default Dashboard;