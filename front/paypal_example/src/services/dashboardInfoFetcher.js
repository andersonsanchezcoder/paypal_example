const DashboardInfoFetcher = async () => {
  // TODO: Crear servicio que realice el fetch en el inicio de sesión, se traiga el toke y lo setee en localstorage
  // let token = localStorage.getItem("jwt");
  let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzg1MjkyMDUsInN1YiI6MiwiZW1haWwiOiJhbmRlcnNvbi5zYW5jaGV6LmNvZGVyQGdtYWlsLmNvbSJ9.g_Ox7sIyjGo4ZR7nz-jSXAahazvErzfIT2miljLUZkQ"
  try{
  let response = await fetch(`http://localhost:3000/dashboard`, { method: 'GET', headers: {'Authorization': token } })
  return await response.json();
  }catch(error){
    localStorage.removeItem('jwt');
  }
}

export default DashboardInfoFetcher