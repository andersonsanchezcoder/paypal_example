class User < ApplicationRecord
  has_secure_password
  # TODO: Analizar si un usuario podria tener varias cuentas
  has_one :account
  validates :email, :username, presence: true

  def to_token_payload
      {
          sub: id,
          email: email
      }
  end
end
