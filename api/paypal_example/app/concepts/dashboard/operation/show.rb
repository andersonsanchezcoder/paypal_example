class Dashboard::Operation::Show < Trailblazer::Operation
  step    :find_user_account
  failure :account_not_found, fail_fast: true
  step    :filter_data

  def find_user_account(options, params, **)
      options['account'] = options[:current_user].account
  end

  def filter_data(options, **)
    options['filtered_account_data'] = {
                                  balance: options['account'].balance
                               }
  end

  def account_not_found(options, **)
    options['message'] = 'Este usuario no posee cuenta'
  end
end