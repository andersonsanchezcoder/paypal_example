class DashboardController < ApplicationController
  before_action :authenticate_user

  def show
    @operation = Dashboard::Operation::Show.(params, current_user: current_user)
    if @operation.success?
      render json: { data: @operation['filtered_account_data'], message: 'Cuenta encontrada' } 
    else
      render json: { message: @operation['message'] }
    end
  end
end
